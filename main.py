from studip_session import connector
from studip_session.connector import AuthError
import console
import re
import time


console.welcome()

# Login
while True:
    username, password = console.get_login_data()
    session = connector.Session()
    try:
        session.login(username, password)
    except AuthError:
        print("There was an error during login! Please try again")
    else:
        break

# Choose course
courses = console.get_courses(session.get_courses())
console.print_courses(courses)
c_id = console.get_course_id(len(courses))

# Select group
groups = console.get_groups(session.get_groups(console.get_data_by_id(courses, c_id)))
console.print_courses(groups)
g_id = console.get_course_id(len(groups))

# Join time
countdown = 0
while True:
    join_time = console.get_join_time()
    server_time = session.get_time(console.get_data_by_id(courses, c_id))
    if join_time[0] < server_time[0]:
        print("Invalid Time!")
        continue
    elif join_time[0] == server_time[0]:
        if join_time[1] <= server_time[1]:
            print("Invalid Time!")
            continue
    countdown += 59 - server_time[2]  # Seconds
    countdown += 60 * (join_time[1] - server_time[1] - 1)  # Minutes
    countdown += 3600 * (join_time[0] - server_time[0])  # Hours
    break

# Wait
print("Waiting for %i seconds!" % countdown)
start = time.time()
time.sleep(countdown)
print("Waited for %i seconds!" % (time.time() - start))
for _ in range(30):
    print("Spam join")
    try:
        session.join_group(console.get_data_by_id(courses, c_id), console.get_data_by_id(groups, g_id))
    except connector.SessionError:
        break
    else:
        time.sleep(0.1)



