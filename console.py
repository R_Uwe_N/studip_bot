def welcome():
    print("Studip-bot version 3.0")


def get_login_data():
    print("Login")
    user = input("Username:\t")
    password = input("Password:\t")

    return user, password


def get_join_time():
    mins = 0
    hour = 0
    while True:
        t = input("Join the group at which time? (hh:mm)\t")
        if len(t) > 5 or t[2] != ":":
            print("Invalid format!")
            continue
        try:
            hour = int(t[0:2])
            mins = int(t[3:5])
        except ValueError:
            print("You need to enter numbers!")
        else:
            break
    return [hour, mins]


def print_courses(data):
    print("=" * 40)
    for i, course in enumerate(data.keys()):
        print("%i. %s" % (i, course))
    print("=" * 40)


def get_courses(data):
    ret = {}
    for d in data:
        if "Feedback-Forum zu" not in d["path"] and "Studiengänge Informatik" not in d["path"]:
            ret[d["path"]] = d["c_id"]

    return ret


def get_course_id(max_number):
    i = 0
    while True:
        try:
            i = int(input("Enter an index:\t"))
        except ValueError:
            print("You need to enter a number!")
        else:
            if i < 0 or i >= max_number:
                print("Invalid index!")
            else:
                return i


def get_groups(data):
    ret = {}
    for x in data:
        if "nogroup" not in x["g_id"]:
            ret[x["name"]] = x["g_id"]
    return ret


def get_data_by_id(data, index):
    for i, d in enumerate(data):
        if i == index:
            return data[d]


def get_key_by_index(data, index):
    for i, d in enumerate(data):
        if i == index:
            return d
