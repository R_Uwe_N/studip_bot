from bs4 import BeautifulSoup
from datetime import datetime
from urllib.parse import urlparse, urlsplit, parse_qs
import html
import re
import time


class ParsingError(Exception):
    pass


def get_sso_url(data):
    soup = BeautifulSoup(data, "html.parser")

    for div in soup.find_all("div", attrs={"class": "login_link"}):
        return div.find("a")["href"]

    raise ParsingError("Could not find the login link")


def get_sso_path(data):
    soup = BeautifulSoup(data, "html.parser")

    for form in soup.find_all("form"):
        if "action" in form.attrs:
            return form.attrs["action"]

    raise ParsingError("Could not find the sso login form")


def get_sso_data(data):
    soup = BeautifulSoup(data, "html.parser")

    saml_response = None
    relay_state = None

    for _input in soup.find_all("input"):
        if "name" in _input.attrs and _input.attrs["name"] == "RelayState":
            relay_state = _input.attrs["value"]
        elif "name" in _input.attrs and _input.attrs["name"] == "SAMLResponse":
            saml_response = _input.attrs["value"]

    return relay_state, saml_response


def get_login(data):
    soup = BeautifulSoup(data, "html.parser")

    for form in soup.find_all("form"):
        if "action" in form.attrs:
            return html.unescape(form.attrs["action"])

    raise ParsingError("Could not find the stud-ip login page form")


def get_footer(data):
    soup = BeautifulSoup(data, "html.parser")

    for div in soup.find_all("div", attrs={"id": "footer"}):
        return div.text

    raise ParsingError("Could not the footer")


def get_courses(data):
    soup = BeautifulSoup(data, "html.parser")
    regex = re.compile(r"https://studip.uni-hannover.de/seminar_main.php\?auswahl=[0-9a-f]*$")

    for link in soup.find_all("a", href=regex):
        href = link.attrs["href"]
        query = urlsplit(href).query
        c_id = parse_qs(query).get("auswahl")[0]

        name = re.sub(r"\s\s+", " ", link.get_text(strip=True))
        name = name.replace("/", "--")
        name = name.replace(":", "_")

        yield {
            "c_id": c_id,
            "path": name
        }


def get_groups(data):
    soup = BeautifulSoup(data, "html.parser")
    for link in soup.find_all("a", class_="get-group-members"):
        g_id = link.attrs["data-group-id"]
        name = re.sub(r"\s\s+", " ", link.get_text(strip=True))
        name = name.replace("/", "--")
        name = name.replace(":", "_")

        yield {
            "g_id": g_id,
            "name": name
        }


def get_time(data):
    soup = BeautifulSoup(data, "html.parser")
    div = soup.find("div", {"id": "footer"})
    regex = re.compile(r"[0-2][0-9]:[0-5][0-9]:[0-5][0-9]")
    server_time = regex.search(div.get_text(strip=True)).group()
    try:
        server_time = [int(server_time[0:2]), int(server_time[3:5]), int(server_time[6:8])]
    except ValueError:
        print("[!] Error during parsing server time!")
    return server_time


def join_group(data):
    return None


def get_token_value(data, name):
    soup = BeautifulSoup(data, "html.parser")

    for tag in soup.find_all(attrs={"name": name}):
        return tag.attrs["value"]

    raise ParsingError("Could not extract ", name)


def construct_sso_url(url, path):
    return "https://" + urlparse(url).netloc + path


def get_timestamp(data):
    soup = BeautifulSoup(data, "html.parser")

    for tr in soup.find_all("tr"):
        if tr.findChildren()[0].getText() == "Geändert":
            timestamp = tr.findChildren()[1].getText()
            d_timestamp = datetime.strptime(timestamp, "%d.%m.%Y %H:%M")
            return int(time.mktime(d_timestamp.timetuple()))
