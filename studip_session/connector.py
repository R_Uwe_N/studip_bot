import requests
from studip_session import parser


class AuthError(Exception):
    pass


class SessionError(Exception):
    pass


class URL:
    url_root = "https://studip.uni-hannover.de"
    login_page = url_root + "/index.php?again=yes&sso=shib"
    courses = url_root + "/dispatch.php/my_courses"
    groups = url_root + "/dispatch.php/course/statusgroups?cid="
    join = url_root + "/dispatch.php/course/statusgroups/join/"


class Session(object):
    def __init__(self, proxies={}, user_agent="PySync", verify_proxy=True):
        self.session = requests.session()
        self.session.headers.update({"User-Agent": user_agent})

        self.session.proxies = proxies

        # SSL Cert Verification - useful for debugging
        self.session.verify = verify_proxy

    def login(self, username, password):
        # print("[*] Accessing StudIP site")
        with self.session.get(URL.login_page) as response:
            if not response.ok:
                raise AuthError("Failed to access login page")

            sso_url = parser.get_sso_url(response.text)

        # print("[*] Getting redirected to the sso login page")
        # follow redirect to get the final web sso login page
        with self.session.get(sso_url) as response:
            if not response.ok:
                raise AuthError("Failed to get redirected to the sso login page")

            sso_path = parser.get_sso_path(response.text)
            sso_login_url = parser.construct_sso_url(response.history[-1].url, sso_path)

        # print("[*] Passing credentials to web sso")
        with self.session.post(sso_login_url, data={"j_username": username,
                                                    "j_password": password,
                                                    "_eventId_proceed": ""}) as response:

            relay_state, saml_response = parser.get_sso_data(response.text)
            final_login_page = parser.get_login(response.text)

        # print("[*] Handing RelayState and SAMLResponse back to StudIP")
        try:
            with self.session.post(final_login_page, data={"RelayState": relay_state,
                                                           "SAMLResponse": saml_response}) as response:
                if not response.ok:
                    raise AuthError("Login failed")

        except requests.exceptions.MissingSchema as exp:  # we got redirected, url doesn't match -> login failed
            raise AuthError("Login Failed!")

    def get_courses(self):
        # print("[*] Fetching course ids...")
        with self.session.get(URL.courses) as response:
            if not response.ok:
                raise SessionError("Could not extract course ids")

            return parser.get_courses(response.text)

    def get_groups(self, course_id):
        # print("[*] Fetching group ids...")
        with self.session.get(URL.groups + course_id) as response:
            if not response.ok:
                raise SessionError("Could not extract group ids")

            return parser.get_groups(response.text)

    def get_time(self, course_id):
        # print("[*] Fetching data...")
        with self.session.get(URL.groups + course_id) as response:
            if not response.ok:
                raise SessionError("Could not fetch the page")

            return parser.get_time(response.text)

    def join_group(self, course_id, group_id):
        # print("[*] Try joining the group...")
        with self.session.get(URL.join + group_id + "?cid=" + course_id) as response:
            if not response.ok:
                raise SessionError("Could not join the group")

            return parser.join_group(response.text)
